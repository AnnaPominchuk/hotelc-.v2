﻿using System;


namespace Hotel.Utils
{
    public abstract class Entity
    {

        /*------------------------------------------------------------------------------------------*/

        public long DatabaseId { get; set; }

        public Guid Id { get; private set; }

    /*------------------------------------------------------------------------------------------*/

        protected Entity(Guid _id)
        {
            if (_id == null)
                throw new ArgumentNullException("Id");

            this.Id = _id;
        }

        protected Entity() { }

        /*------------------------------------------------------------------------------------------*/

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj == null || GetType() != obj.GetType())
                return false;

            var otherEntity = (Entity)obj;
            return Id == otherEntity.Id;
        }

    /*------------------------------------------------------------------------------------------*/

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

    /*------------------------------------------------------------------------------------------*/
    }
}