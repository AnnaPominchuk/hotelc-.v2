﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class GuestProcessingDto : DomainEntityDto<GuestProcessingDto>
    {
        public OrderDto CurrentOrder { get; private set; }

        public IList<ServiceDto> Services { get; private set; }


        public GuestProcessingDto(Guid domainId,
                          OrderDto CurrentOrder,
                          IList<ServiceDto> Services)

            : base(domainId)
        {
            this.CurrentOrder = CurrentOrder;
            this.Services = Services;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>()
            {
                DomainId,
                CurrentOrder

            };

            foreach (var item in Services)
                list.Add(item);

            return list;
        }

        public override string ToString()
        {
            return string.Format(
                  "OrderId = {0}\nCurrentOrder:\n{1}Services = {2}\n",
                  DomainId,
                  CurrentOrder,
                  string.Join("\n", Services)
            );
        }
    }
}