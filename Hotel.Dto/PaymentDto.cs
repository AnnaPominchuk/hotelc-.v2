﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class PaymentDto : DomainEntityDto<PaymentDto>
    {
        public bool Exist { get; set; }

        public int Price { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public PaymentDto(Guid domainId, int price, bool exist )
            : base (domainId)
        {
            Exist = exist;
            Price = price;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>()
            {
                Price,
                Exist
            };

            return list;
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                "\nPrice : {0}",
                 Price
           );
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
