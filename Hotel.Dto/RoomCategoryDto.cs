﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class RoomCategoryDto : DomainEntityDto<RoomCategoryDto>
    {
        public int RoomsNumber { get; private set; }

        public int BathroomsNumber { get; private set; }

        public int SingleBedsNumber { get; private set; }

        public int BedsNumber { get; private set; }

        public string Name { get; private set; }

        public ICollection<RoomDto> Rooms;

        public RoomCategoryDto(
             Guid id
           , int roomsNumber
           , int bathroomsNumber
           , int singleBedsNumber
           , int bedsNumber
           , string name
        )
            : base(id)
        {
            Rooms = new List<RoomDto>();

            RoomsNumber = roomsNumber;
            BathroomsNumber = bathroomsNumber;
            SingleBedsNumber = singleBedsNumber;
            BedsNumber = bedsNumber;
            Name = name; 
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>()
            {
                DomainId,
                RoomsNumber,
                BathroomsNumber,
                SingleBedsNumber,
                BedsNumber
            };

            return list;
        }

        public override string ToString()
        {
            return string.Format(
                "nRoomsNumber = {0}\nBathroomsNumber = {1}\nsingleBedsNumber = {2}\nBedsNumber = {3}",
                 RoomsNumber,
                 BathroomsNumber,
                 SingleBedsNumber,
                 BedsNumber
           );
        }
    }
}