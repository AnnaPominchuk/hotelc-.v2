﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class RoomDto : DomainEntityDto<RoomDto>
    {
        public string ImageUrl { get; private set; }

        public int Price { get; private set; }

        public int Square { get; private set; }

        public string Status { get; private set; }

        public RoomDto(Guid domainId,
                          string ImageUrl,
                          int Price,
                          int Square,
                          string Status)

            : base(domainId)
        {
            this.ImageUrl = ImageUrl;
            this.Price = Price;
            this.Square = Square;
            this.Status = Status;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>()
            {
                DomainId,
                ImageUrl,
                Price,
                Square,
                Status
            };

            return list;
        }

        public override string ToString()
        {
            return string.Format(
                  "OrderId = {0}\nImageUrl:\n{1}nPrice = {2}\nSquaree = {3}\nStatus = {4}\n",
                  DomainId,
                  ImageUrl,
                  Price,
                  Square,
                  Status
            );
        }
    }
}