﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class TaskDto : DomainEntityDto<TaskDto>
    {
        public GuestProcessingDto processing { get; private set; }

        public ServiceDto service { get; private set; }

        public string Status { get; private set; }

        public TaskDto(Guid domainId,
                          GuestProcessingDto processing,
                          ServiceDto service,
                          string Status)
            : base(domainId)
        {
            this.processing = processing;
            this.service = service;
            this.Status = Status;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>()
            {
                DomainId,
                processing,
                service,
                Status
            };

            return list;
        }

        public override string ToString()
        {
            return string.Format(
                  "OrderId = {0}\nprocessing = {1}\nservice = {2}\nStatus = {3}\n",
                  DomainId,
                  processing,
                  service,
                  Status
            );
        }
    }
}