﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class ServiceDto : DomainEntityDto<ServiceDto>
    {
        public string Category { get; private set; }

        public int Price { get; private set; }

        public ServiceDto(Guid domainId,
                          string Category,
                          int Price )
            : base(domainId)
        {
            this.Category = Category;
            this.Price = Price;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>()
            {
                DomainId,
                Category,
                Price
            };

            return list;
        }

        public override string ToString()
        {
            return string.Format(
                  "OrderId = {0}\nCategory:{1}\nPrice = {2}\n",
                  DomainId,
                  Category,
                  Price
            );
        }
    }
}