﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class AccountDto : DomainEntityDto<AccountDto>
    {
        public string Name { get; private set; }

        public string Login { get; private set; }

        public string TelephonNumber { get; set; }

        public AccountDto(Guid domainId, string name, string login, string telephonNumber)
            : base(domainId)
        {
            this.Name = name;
            this.Login = login;
            this.TelephonNumber = telephonNumber;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object> { DomainId, Name, Login };
        }

        public override string ToString()
        {
            return string.Format("AccountId = {0}\nName = {1}\nLogin = {2}\nTelephon Number = {3}\n", DomainId, Name, Login, TelephonNumber);
        }
    }
}