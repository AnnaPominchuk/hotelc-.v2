﻿using System;
using System.Collections.Generic;

namespace Hotel.Dto
{
    public class OrderDto : DomainEntityDto<OrderDto>
    {
        public DateTime startTime { get; private set; }

        public DateTime endTime { get; private set; }

        public string Status { get; private set; }

        public RoomDto room { get; private set; }


        public OrderDto(Guid domainId,
                          DateTime startTime,
                          DateTime endTime,
                          string status,
                          RoomDto room)

            : base(domainId)
        {
            this.startTime = startTime;
            this.endTime = endTime;
            this.Status = status;
            this.Status = status;
            this.room = room;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object>()
            {
                DomainId,
                room,
                Status,
                startTime,
                endTime
            };

            return list;
        }

        public override string ToString()
        {
            return string.Format(
                  "OrderId = {0}\nRoom:\n{1}Status = {2}\nstartTime = {3}\nendTime = {4}\n",
                  DomainId,
                  room,
                  Status,
                  startTime,
                  endTime
            );
        }
    }
}