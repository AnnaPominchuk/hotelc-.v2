﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class RoomCategory : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public int RoomsNumber { get; set; }
        public int BathroomsNumber { get; set; }
        public int SingleBedsNumber { get; set; }
        public int BedsNumber { get; set; }

        public string Name { get; set; }

        public ICollection<Room> Rooms;

        /*------------------------------------------------------------------------------------------*/

        public RoomCategory(
                Guid id
            ,   int roomsNumber
            ,   int bathroomsNumber
            ,   int singleBedsNumber
            ,   int bedsNumber
            ,   string name
        )
            : base( id )
        {
            Rooms = new List<Room>();

            RoomsNumber = roomsNumber;
            BathroomsNumber = bathroomsNumber;
            SingleBedsNumber = singleBedsNumber;
            BedsNumber = bedsNumber;
            Name = name;
        }

        protected RoomCategory()
        {
            Rooms = new List<Room>();
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                "nRoomsNumber = {0}\nBathroomsNumber = {1}\nsingleBedsNumber = {2}\nBedsNumber = {3}",
                 RoomsNumber,
                 BathroomsNumber,
                 SingleBedsNumber,
                 BedsNumber
           );
        }

        /*------------------------------------------------------------------------------------------*/

        public void changeBedsNumber(int _bedsNumber)
        {
            BedsNumber = _bedsNumber;
        }

        public void changeSingleBedsNumber(int _singleBedsNumber)
        {
            SingleBedsNumber = _singleBedsNumber;
        }

        /*------------------------------------------------------------------------------------------*/

    }
}
