﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class Service : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public enum ServiceCategory
        {
            drinking,
            eating,
            spa
        };

        static public ServiceCategory toEnum( string category)
        {
            switch(category)
            {
                case "drinking": return ServiceCategory.drinking;
                case "eating": return ServiceCategory.eating;
                case "spa": return ServiceCategory.spa;

                default: return ServiceCategory.drinking;
            }
        }

        /*------------------------------------------------------------------------------------------*/

        public ServiceCategory Category { get; set; }
        public int Price { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public Service(
                Guid id
            ,   ServiceCategory category
            ,   int price
        )
            : base( id )
        {
            Category = category;
            Price = price;
        }

        protected Service() { }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                "Category = {0}\nPrice: {1}",
                 Category,
                 Price
           );
        }
    }
}
