﻿using System;
using System.Collections.Generic;

namespace Hotel.Model
{
    public class Account : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public string Name { get; set; }
        
        public string Login { get; set; }

        public string Password { get; set; }

        public string TelephonNumber { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public Account( Guid id, string name, string login, string password, string telephonNumber)
            :   base(id)
        {
            Name = name;
            Login = login;
            TelephonNumber = telephonNumber;
            ChangePassword(password);
        }

        protected Account()
        {}

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                "ID = {0}\nName = {1}\nLogin: {2}\nPassword = {3}",
                 Id,
                 Name,
                 Login,
                 Password
           );
        }

        /*------------------------------------------------------------------------------------------*/

        public bool CheckPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            return Password == password;
        }

        public void ChangePassword(string password)
        {
            Password = password;
        }

        /*------------------------------------------------------------------------------------------*/

    }
}
