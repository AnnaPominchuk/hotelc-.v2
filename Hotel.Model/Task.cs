﻿using System;
using System.Collections.Generic;


namespace Hotel.Model
{
    public class Task : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public enum Status
        {   processing,
            closed,
            unassigned // not started
        };

        /*------------------------------------------------------------------------------------------*/

        public GuestProcessing processing { get; set; }
	    public Service service { get; set; }
        public Status status { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public Task (Guid id, GuestProcessing _processing, Service _service )
            :   base( id )
        {
            processing = _processing;
            service = _service;
            status = Status.unassigned;
        }

        public void progress()
        {
            status = Status.processing;
        }

        public void close()
        {
            status = Status.closed;
        }

        protected Task() { }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nGuestProcessing: {1}\nService = {2}",
                       Id,
                       processing.Id,
                       service.Id
                       );

        }

        /*------------------------------------------------------------------------------------------*/
    }
}
