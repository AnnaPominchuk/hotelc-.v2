﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class StaffAccount : Account
    {
        /*------------------------------------------------------------------------------------------*/

        public StaffAccount(Guid id, string name, string login, string passwordHash, string telephonNumber)
            : base(id, name, login, passwordHash, telephonNumber)
        {
            this.Tasks = new List<Task>();
        }

        protected StaffAccount()
        {
            this.Tasks = new List<Task>();
        }

        /*------------------------------------------------------------------------------------------*/

        public ICollection<Task> Tasks { get; private set; }

        /*------------------------------------------------------------------------------------------*/
    }
}
