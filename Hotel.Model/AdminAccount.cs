﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class AdminAccount : Account
    {
        /*------------------------------------------------------------------------------------------*/

        public AdminAccount(Guid id, string name, string login, string passwordHash, string telephonNumber)
            : base(id, name, login, passwordHash, telephonNumber)
        {
            this._orders = new List<Order>();
        }

        protected AdminAccount()
        {
            this._orders = new List<Order>();
        }

        /*------------------------------------------------------------------------------------------*/

        public void MeetGuest(ClientAccount _guest, DateTime _today, int _number)
        {
          // TO DO
        }

        /*------------------------------------------------------------------------------------------*/

        public ICollection<Order> _orders { get; private set; }

        /*------------------------------------------------------------------------------------------*/
    }
}
