﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class ClientAccount : Account
    {

        /*------------------------------------------------------------------------------------------*/

        public ClientAccount(Guid id, string name, string login, string passwordHash, string telephonNumber)
            : base(id, name, login, passwordHash, telephonNumber)
        {
            this._orders = new List<Order>();
        }

        protected ClientAccount()
        {
            this._orders = new List<Order>();
        }

        /*------------------------------------------------------------------------------------------*/

        public void TrackOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            _orders.Add(order);
        }

        /*------------------------------------------------------------------------------------------*/

        public ICollection<Order> _orders { get; set; }

        /*------------------------------------------------------------------------------------------*/
    }
}
