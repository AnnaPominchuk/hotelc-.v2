﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class Order : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public enum OrderStatus
        {
            booked,
            executing,
            canceled
        }

        /*------------------------------------------------------------------------------------------*/

        public OrderStatus Status { get; private set; }

        public DateTime startTime { get; private set; }

        public DateTime endTime { get; private set; }

        public Room room { get; private set; }

     //   public Payment payment { get; private set; }


        /*------------------------------------------------------------------------------------------*/

        public Order(
                Guid id
            ,   DateTime starttime
            ,   DateTime endtime
            ,   Room _room
        )
            : base(id)
        {
            Status = OrderStatus.booked;
            this.startTime = starttime;
            this.endTime = endtime;

            this.room = _room;
        }

        protected Order()
        {
           
        }

        /*------------------------------------------------------------------------------------------*/

        public void Confirm()
        {
            Status = OrderStatus.executing;
        }

        public void Cancel()
        {
            Status = OrderStatus.canceled;
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nStatus = {1}\ntime = {2}\nRoom = {3}",
                       Id,
                       Status,
                       startTime,
                       room.Id
                   );
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
