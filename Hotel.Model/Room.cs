﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class Room : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public enum RoomStatus
        {
                need_cleaning
            ,   do_not_disturb
            ,   undefined
        }

        /*------------------------------------------------------------------------------------------*/

        public string ImageUrl { get; set; }

        public int Price { get; set; }

        public int Square { get; private set; }

        public RoomStatus Status { get; private set; }

        /*------------------------------------------------------------------------------------------*/

        public Room(
              Guid id
            , int square
            , string imageUrl
            , int price
        )
            : base(id)
        {
            ImageUrl = imageUrl;
            Price = price;
            Square = square;
            Status = RoomStatus.undefined;
        }

        protected Room() { }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nImageUrl = {1}\nPrice = {2}\nSquare = {3}\n",
                       Id,
                       ImageUrl,
                       Price,
                       Square
                   );
        }

        /*------------------------------------------------------------------------------------------*/

        public void ChangePrice(int price)
        {
            Price = price;
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
