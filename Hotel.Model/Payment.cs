﻿using System;
using System.Collections.Generic;

namespace Hotel.Model
{
    public class Payment : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public enum PaymentMethod
        {
            cash,
            cashless
        }

        /*------------------------------------------------------------------------------------------*/

        public PaymentMethod Status { get; set; }

        public bool Exist { get; set; }

        public int Price { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public Payment(Guid id) : base(id) { }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                "\nPrice : {0}",
                 Price
           );
        }

        /*------------------------------------------------------------------------------------------*/

        public void makePayment()
        {
            Exist = true;
        }

        public bool checkPayment()
        {
            return Exist;
        }

        /*------------------------------------------------------------------------------------------*/

        public void Confirm()
        {
            Exist = true;
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
