﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Model
{
    public class GuestProcessing : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public Order CurrentOrder { get; private set; }

        public ICollection<Service> Services { get; private set; }

        /*------------------------------------------------------------------------------------------*/

        public GuestProcessing( Guid id, Order order)
            :   base ( id )
        {
            CurrentOrder = order;
            Services = new List<Service>();
        }

        protected GuestProcessing()
        {
            Services = new List<Service>();
        }

        /*------------------------------------------------------------------------------------------*/


    }
}
