﻿using System;
using Hotel.Dto;
using Hotel.Service.Validators;

namespace Hotel.Service
{
    public interface IRoomCategoryService : IDomainEntityService<RoomCategoryDto>
    {
        void ChangeBedsNumber(Guid roomCategory, [IntegerPositiveValidator] int newBedsNumber);

        void ChangeSingleBedsNumber(Guid roomCategory, [IntegerPositiveValidator] int newSingleBedsNumber);

        Guid Create(
                [IntegerPositiveValidator] int roomsNumber
            , [IntegerPositiveValidator] int bathroomsNumber
            , [IntegerPositiveValidator] int singleBedsNumber
            , [IntegerPositiveValidator] int bedsNumber
            , string name
        );

        void addRoom(Guid roomCategory, Guid room);

        //Guid AddInfoCard(
        //      [IntegerPositiveValidator] int RoomsNumber
        //    , [IntegerPositiveValidator] int BathroomsNumber
        //    , [IntegerPositiveValidator] int SingleBedsNumber
        //    , [IntegerPositiveValidator] int BedsNumber
        //    , string name);

    }

}
