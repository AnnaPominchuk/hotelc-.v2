﻿using System;
using Hotel.Dto;
using Hotel.Service.Validators;

namespace Hotel.Service
{
    public interface IAccountService : IDomainEntityService<AccountDto>
    {
        AccountDto Identify([EmailValidator] string login, [NonEmptyStringValidator] string password);

        Guid RegisterClient(
                [NonEmptyStringValidator] string name
            ,   [NonEmptyStringValidator] string login
            ,   [NonEmptyStringValidator]  string password
            ,   [PhoneValidator] string phone
         );

        Guid RegisterAdmin(
                [NonEmptyStringValidator] string name
            ,   [NonEmptyStringValidator] string login
            ,   [NonEmptyStringValidator] string password
            ,   [PhoneValidator] string phone
        );

        Guid RegisterStaff(
                [NonEmptyStringValidator] string name
            ,   [NonEmptyStringValidator] string login
            ,   [NonEmptyStringValidator] string password
            ,   [PhoneValidator] string phone
       );

        void ChangeName(Guid accountId, [NonEmptyStringValidator] string newName);

        void DeleteAccount(Guid accountId);

        void connectCustomerOrder(Guid customerId, Guid orderId);
    }

}
