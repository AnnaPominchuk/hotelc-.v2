﻿using Hotel.Dto;
using System;
using System.Collections.Generic;

namespace Hotel.Service
{
    public interface IDomainEntityService<TDto> where TDto : DomainEntityDto<TDto>
    {
        IList<Guid> ViewAll();

        TDto View(Guid domainId);
    }
}