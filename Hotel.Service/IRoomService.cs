﻿using System;
using Hotel.Dto;
using Hotel.Service.Validators;

namespace Hotel.Service
{
    public interface IRoomService : IDomainEntityService<RoomDto>
    {
        void AddPhoto(Guid id, string Url);

        void ChangePrice(Guid id, [IntegerPositiveValidator] int price);

        Guid Create([IntegerPositiveValidator] int square, string imageUrl, [IntegerPositiveValidator] int price);

        RoomDto ShowRoomInfo(Guid id);
    }
}


