﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Hotel.Service.Validators
{

    [AttributeUsage(AttributeTargets.Property |
                      AttributeTargets.Field |
                      AttributeTargets.Parameter)
    ]
    public class NonEmptyStringValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator(Type targetType)
        {
            return new StringLengthValidator(
                             0,
                             RangeBoundaryType.Exclusive,
                             int.MaxValue,
                             RangeBoundaryType.Ignore
                       );
        }
    }
}

