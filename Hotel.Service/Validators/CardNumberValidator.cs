﻿using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Hotel.Service.Validators
{

    [AttributeUsage(AttributeTargets.Property |
                      AttributeTargets.Field |
                      AttributeTargets.Parameter)
    ]
    public class CardNumberValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator(Type targetType)
        {
            const string CardNumberRegex = @"[0-9](\d{15})";
            return new RegexValidator(CardNumberRegex);
        }
    }
}

