﻿using System;
using Hotel.Dto;
using Hotel.Service.Validators;

namespace Hotel.Service
{
    public interface IPaymentService : IDomainEntityService<PaymentDto>
    {
        void SelectPaymentMethod(Guid id, [NonEmptyStringValidator] string method);

        bool SpecifyCardInfo([CardNumberValidator] string CardNumber, string CVVcode);

        void ConfirmCardPayment(Guid id);
    }
}
