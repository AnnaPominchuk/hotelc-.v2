﻿using System;
using Hotel.Dto;

namespace Hotel.Service
{
    public interface IOrderService : IDomainEntityService<OrderDto>
    {
        void ConfirmOrderPlacement(Guid id);

        // void SpecifyCardInfo(Guid id);

        Guid Create( DateTime starttime, DateTime endtime, Guid _room);

        void CancelReservation(Guid id);
    }
}
