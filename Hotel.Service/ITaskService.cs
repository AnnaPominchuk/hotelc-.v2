﻿using System;
using Hotel.Dto;

namespace Hotel.Service
{
    public interface ITaskService : IDomainEntityService<TaskDto>
    {
        string ViewStatus (Guid id);

        Guid AddNewTask( GuestProcessingDto _processing, ServiceDto _service);

        void ProgressTask(Guid id);

        void CloseTask(Guid id);
    }
}
