﻿using System;
using Hotel.Dto;

namespace Hotel.Service
{
    public interface IGuestProcessingService 
        :   IDomainEntityService<GuestProcessingDto>
    {
        TaskDto OrderDishes( Guid id );

        TaskDto OrderDrinks( Guid id );
    }
}
