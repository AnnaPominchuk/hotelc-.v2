﻿using System;

namespace Photostudio.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                PhotostudioModel model = TestModelGenerator.GenerateTestData();

                ModelReporter reporter = new ModelReporter(Console.Out);
                reporter.GenerateReport(model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType().FullName);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
