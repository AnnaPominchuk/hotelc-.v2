﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Photostudio.TestApp
{
    class ModelReporter
    {
        public ModelReporter(TextWriter output)
        {
            this.output = output;
        }

        public void GenerateReport(PhotostudioModel model)
        {
            ReportCollection("Operator Accounts", model.OperatorAccounts);
            ReportCollection("Photograph Accounts", model.PhotographAccounts);
            ReportCollection("Clients ", model.ClientAccounts);
            ReportCollection("Studio", model.Studios);
            ReportCollection("Rooms", model.Rooms);
            ReportCollection("Orders", model.Orders);
        }

        private void ReportCollection<T>(string title, ICollection<T> items)
        {
            output.WriteLine("==== {0} ==== ", title);
            output.WriteLine();

            foreach (var item in items)
            {
                output.WriteLine(item);
                output.WriteLine();
            }

            output.WriteLine();
        }

        private TextWriter output;
    }
}
