﻿using Photostudio.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.TestApp
{
    class PhotostudioModel
    {
        public List<Account> OperatorAccounts { get; private set; }

        public List<PhotographAccount> PhotographAccounts { get; private set; }

        public List<Account> ClientAccounts { get; private set; }

        public List<Studio> Studios { get; private set; }

        public List<Room> Rooms { get; private set; }

        public List<Order> Orders { get; private set; }

        public PhotostudioModel()
        {
            this.OperatorAccounts = new List<Account>();
            this.PhotographAccounts = new List<PhotographAccount>();
            this.ClientAccounts = new List<Account>();
            this.Rooms = new List<Room>();
            this.Orders = new List<Order>();
            this.Studios = new List<Studio>();
        }

    }
}
