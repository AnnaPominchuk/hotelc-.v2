﻿using Photostudio.Model;

using System;
using System.Collections.Generic;

namespace Photostudio.TestApp
{
    static class TestModelGenerator
    {
        public static PhotostudioModel GenerateTestData()
        {
            PhotostudioModel model = new PhotostudioModel();

            GenerateAccounts(model);
            GenerateRoomes(model);
            GenerateOrders(model);
            GenerateStudio(model);

            return model;
        }


        private static void GenerateAccounts(PhotostudioModel m)
        {
            Contact contact1 = new Contact( "38095478", "karina@gmail.com" );
            OperatorAccount operator1 = new OperatorAccount(Guid.NewGuid(), "Karina", contact1, "12345");
            m.OperatorAccounts.Add(operator1);

            Contact contact2 = new Contact("4675687", "masha@gmail.com");
            string photogtaphDescription = "Family photogtaph";
            PhotographAccount photogtaph1 = new PhotographAccount(Guid.NewGuid(), "Masha", contact2, "12345", photogtaphDescription, 300);

            Contact contact3 = new Contact("7669843", "sasha@gmail.com");
            string photogtaphDescription2 = "Family photogtaph";
            PhotographAccount photogtaph2 = new PhotographAccount(Guid.NewGuid(), "Sasha", contact3, "12345", photogtaphDescription2, 500);

            photogtaph2.AddVacationInSchedule("vacation", "New Year", new DateTime( 2018, 1, 1 ));

            m.PhotographAccounts.Add(photogtaph1);
            m.PhotographAccounts.Add(photogtaph2);

            Contact contact4 = new Contact("4587634", "anna@gmail.com");
            ClientAccount customer = new ClientAccount(Guid.NewGuid(), "Anna", contact4, "12345");
            m.ClientAccounts.Add(customer);
        }

        private static void GenerateRoomes(PhotostudioModel m)
        {
            string roomDescription1 = "Family room";
            Room room1 = new Room(Guid.NewGuid(), "Lenina 1", roomDescription1, 500);

            string roomDescription2 = "Family room";
            Room room2 = new Room(Guid.NewGuid(), "Lenina 2", roomDescription2, 300);

            m.Rooms.Add(room1);
            m.Rooms.Add(room2);
        }

        private static void GenerateOrders(PhotostudioModel m)
        {
            order = new Order(
                Guid.NewGuid(),
                new Contact("46576", "contact@gmail.com"),
                DateTime.Now,
                m.PhotographAccounts[0],
                m.Rooms[0]
            );

            m.PhotographAccounts[0].AddOrderInSchedule(order, "Photographing", "Family Photographing", DateTime.Now);
            order.Confirm();

            m.Orders.Add(order);
        }

        private static void GenerateStudio(PhotostudioModel m)
        {
            studio = new Studio(
                Guid.NewGuid(),
                new Studio.WorkTime(9, 0),
                new Studio.WorkTime(20, 0)
              );

            studio.addDayOff( new DateTime( 2017, 5, 22 ) );

            m.Studios.Add(studio);
        }

        private static Order order;
        private static Studio studio;
    }
}
