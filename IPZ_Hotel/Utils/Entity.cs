﻿using System;


namespace Photostudio.Utils
{
    public abstract class Entity
    {

    /*------------------------------------------------------------------------------------------*/

        public Guid Id { get; private set; }

    /*------------------------------------------------------------------------------------------*/

        protected Entity(Guid _id)
        {
            if (_id == null)
                throw new ArgumentNullException("Id");

            this.Id = _id;
        }

    /*------------------------------------------------------------------------------------------*/

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;

            if (obj == null || GetType() != obj.GetType())
                return false;

            var otherEntity = (Entity)obj;
            return Id == otherEntity.Id;
        }

    /*------------------------------------------------------------------------------------------*/

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

    /*------------------------------------------------------------------------------------------*/
    }
}
