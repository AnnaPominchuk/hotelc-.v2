﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class Order : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public enum OrderStatus
        {
            New,
            Confirmed,
            Cancelled,
            InProgress,
            Completed
        }

        /*------------------------------------------------------------------------------------------*/

        public int TotalCost { get; private set; }

        public OrderStatus Status { get; private set; }

        public DateTime PhotographyTime { get; private set; }

        public Room Location { get; private set; }

        public PhotographAccount Photograph { get; private set; }

        public Contact CustomerContact { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public Order(
                Guid id
            ,   Contact contact
            ,   DateTime time
            ,   PhotographAccount photograph
            ,   Room location
        )
            : base(id)
        {
            CustomerContact = contact;
            Status = OrderStatus.New;
            PhotographyTime = time;

            TotalCost = photograph.Price + location.Price ;

            Photograph = photograph;
            Location = location;
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nTotal cost = {1}\nStatus = {2}\nPhotography time = {3}\nCustomer contact : {4}\nPhotograph ={5}\nRoom = {6}",
                       Id,
                       TotalCost,
                       Status,
                       PhotographyTime,
                       CustomerContact.ToString(),
                       Photograph.Id,
                       Location.Id
                   );
        }

        /*------------------------------------------------------------------------------------------*/

        public void Confirm()
        {
            if (Status != OrderStatus.New)
                throw new InvalidOperationException("Order.Confirm - can only run in New state");

            Status = OrderStatus.Confirmed;
        }

        public void Cancel()
        {
            if (Status != OrderStatus.New || Status != OrderStatus.Confirmed)
                throw new InvalidOperationException("Order.Cancel - can only run in New state or Confirmed state");

            Status = OrderStatus.Cancelled;
        }

        public void StartProccessing()
        {
            if (Status == OrderStatus.Confirmed)
                Status = OrderStatus.InProgress;
            else
                throw new InvalidOperationException("Order.InProgress - can only happen in Confirmed state");

        }

        public void ProccessingCompleted()
        {
            if (Status == OrderStatus.InProgress)
                Status = OrderStatus.Completed;
            else
                throw new InvalidOperationException("Order.InProgress - can only happen in Delivering state"); 
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
