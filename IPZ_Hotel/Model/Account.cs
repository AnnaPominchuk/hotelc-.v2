﻿using System;
using System.Collections.Generic;

namespace Photostudio.Model
{
    public class Account : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public string Name { get; set; }
        
        public Contact ContactData { get; set; }

        public string Password { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public Account( Guid id, string name, Contact contact, string password )
            :   base(id)
        {
            Name = name;
            ContactData = contact;
            ChangePassword(password);
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                "ID = {0}\nName = {1}\nContactData: {2}\nPassword = {3}",
                 Id,
                 Name,
                 ContactData.ToString(),
                 Password
           );
        }

        /*------------------------------------------------------------------------------------------*/

        public bool CheckPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            return Password == password;
        }

        public void ChangePassword(string password)
        {
            Password = password;
        }

        /*------------------------------------------------------------------------------------------*/

    }
}
