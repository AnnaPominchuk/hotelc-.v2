﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class OperatorAccount : Account
    {
        /*------------------------------------------------------------------------------------------*/

        public ICollection<Order> Orders
        {
            get
            {
                return _orders.AsReadOnly();
            }
        }

        /*------------------------------------------------------------------------------------------*/

        public OperatorAccount(Guid id, string name, Contact contactData, string passwordHash)
            : base(id, name, contactData, passwordHash)
        {
            this._orders = new List<Order>();
        }

        /*------------------------------------------------------------------------------------------*/

        public void TrackOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            _orders.Add(order);
        }

        /*------------------------------------------------------------------------------------------*/

        private readonly List<Order> _orders;

        /*------------------------------------------------------------------------------------------*/
    }
}
