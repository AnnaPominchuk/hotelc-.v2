﻿using System;
using System.Collections.Generic;


namespace Photostudio.Model
{
    public class Studio : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public struct WorkTime
        {
            public int hour;
            public int minutes;

            public WorkTime(int _hour, int _minutes)
            {
                hour = _hour;
                minutes = _minutes;
            }
        }

        /*------------------------------------------------------------------------------------------*/

        public WorkTime openingTime { get; private set; }

        public WorkTime closingTime { get; private set; }

        public List<DateTime> specialSchedule { get; private set; }

        /*------------------------------------------------------------------------------------------*/

        public Studio (Guid id, WorkTime _openingTime, WorkTime _closingTime)
            :   base( id )
        {
            openingTime = _openingTime;
            closingTime = _closingTime;

            specialSchedule = new List<DateTime>();
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            string result = "";
            if (specialSchedule.Count > 0)
            {
                result = "\nDays with spetial schedule:\n";
                foreach (var item in specialSchedule)
                {
                    result += (item.ToString() + "\n");
                }
            }

            return string.Format(
                       "ID = {0}\nStuio worktime: {1}:{2} - {3}:{4}",
                       Id,
                       openingTime.hour,
                       openingTime.minutes,
                       closingTime.hour,
                       closingTime.minutes
                   ) + result;
        }

        /*------------------------------------------------------------------------------------------*/

        public void changeStandartWorkTime(WorkTime newOpeningTime, WorkTime newClosingTime )
        {
            openingTime = newOpeningTime;
            closingTime = newClosingTime;
        }

        /*------------------------------------------------------------------------------------------*/

        public void addDayOff( DateTime day )
        {
           specialSchedule.Add( day );
        }

        /*------------------------------------------------------------------------------------------*/

        public void clearDayOff()
        {
            specialSchedule.Clear();
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
