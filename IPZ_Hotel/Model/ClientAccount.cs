﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class ClientAccount : Account
    {
        /*------------------------------------------------------------------------------------------*/

        public ICollection<Order> Orders
        {
            get
            {
                return _orders.AsReadOnly();
            }
        }

        /*------------------------------------------------------------------------------------------*/

        public ClientAccount(Guid id, string name, Contact contact, string passwordHash)
            : base(id, name, contact, passwordHash)
        {
        }

        /*------------------------------------------------------------------------------------------*/

        public void TrackOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            _orders.Add(order);
        }

        /*------------------------------------------------------------------------------------------*/

        private readonly List<Order> _orders;

        /*------------------------------------------------------------------------------------------*/
    }
}
