﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class ScheduleItem
    {
        /*------------------------------------------------------------------------------------------*/

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime Time { get; set; }

        /*------------------------------------------------------------------------------------------*/

        protected ScheduleItem(
                string title
            ,   string description
            ,   DateTime time
        )
        {
            Description = description;
            Title = title;
            Time = time;
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
