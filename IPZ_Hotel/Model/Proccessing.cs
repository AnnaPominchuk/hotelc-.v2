﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class Proccessing
    {
        /*------------------------------------------------------------------------------------------*/

        public int ProgressStatus { get; private set; }

        public int Priority { get; private set; }

        public Order CurrentOrder { get; private set; }

        /*------------------------------------------------------------------------------------------*/

        public Proccessing( int priority, Order order)
        {
            ProgressStatus = 0;
            CurrentOrder = order;
        }

        /*------------------------------------------------------------------------------------------*/

        public void changePriority( int priority )
        {
            Priority = priority;
        }

        public void changeProgressStatus( int status )
        {
            ProgressStatus = status;
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
