﻿using System;
using System.Collections.Generic;

namespace Photostudio.Model
{
    public class Contact
    {
        /*------------------------------------------------------------------------------------------*/

        public string Phone { get; set; }

        public string Email { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public Contact(string phone, string email)
        {
            Phone = phone;
            Email = email;
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                "\nPhone : {0}\nEmail : {1}",
                 Phone,
                 Email
           );
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
