﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class PhotographAccount : Account
    {
        /*------------------------------------------------------------------------------------------*/

        public string Description { get; private set; }

        public int Price { get; private set; }

        public SortedSet<ScheduleItem> Schedule { get; private set; }

        public List<Proccessing> Queue { get; private set; }

        /*------------------------------------------------------------------------------------------*/

        public PhotographAccount(
                Guid id
            ,   string name
            ,   Contact contact
            ,   string passwordHash
            ,   string description
            ,   int price
        )
            : base(id, name, contact, passwordHash)
        {
            Description = description;
            Price = price;
            Schedule = new SortedSet<ScheduleItem>();
            Queue = new List<Proccessing>();
        }

        /*------------------------------------------------------------------------------------------*/

        public void ChangePrice(int price)
        {
            Price = price;
        }

        public void EditDescription(string description)
        {
            Description = description;
        }

        public void AddOrderInSchedule(Order order, string title, string description, DateTime time)
        {
            Schedule.Add( new OrderScheduleItem(order, title, description, time) );
        }

        public void AddVacationInSchedule(string title, string description, DateTime time)
        {
            Schedule.Add(new CustomScheduleItem(title, description, time));
        }

        public void clearSchedule()
        {
            Schedule.Clear();
        }

        public void AddNewQueueItem(int priotity, Order order )
        {
            Queue.Add(new Proccessing(priotity, order ));
        }

        public void clearQueue()
        {
            Queue.Clear();
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
