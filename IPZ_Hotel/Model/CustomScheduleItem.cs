﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class CustomScheduleItem : ScheduleItem
    {
        /*------------------------------------------------------------------------------------------*/

        public CustomScheduleItem(
                string title
            ,   string description
            ,   DateTime time
        )
            : base( title, description, time )
        {
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
