﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class OrderScheduleItem : ScheduleItem
    {
        /*------------------------------------------------------------------------------------------*/

        public Order order { get; set; }

        /*------------------------------------------------------------------------------------------*/

        public OrderScheduleItem(
                Order _order
            ,   string title
            ,   string description
            ,   DateTime time
        )
            : base( title, description, time )
        {
            order = _order;
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
