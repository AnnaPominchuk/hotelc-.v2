﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photostudio.Model
{
    public class Room : Utils.Entity
    {
        /*------------------------------------------------------------------------------------------*/

        public string Description { get; private set; }

        public int Price { get; private set; }

        public string Adress { get; private set; }

        /*------------------------------------------------------------------------------------------*/

        public Room(
              Guid id
            , string adress
            , string description
            , int price
        )
            : base(id)
        {
            Description = description;
            Price = price;
            Adress = adress;
        }

        /*------------------------------------------------------------------------------------------*/

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\n Description:{1}\nPrice = {2}\nAdress = {3}\n",
                       Id,
                       Description,
                       Price,
                       Adress
                   );
        }

        /*------------------------------------------------------------------------------------------*/

        void ChangePrice(int price)
        {
            Price = price;
        }

        void EditDescription(string description)
        {
            Description = description;
        }

        void EditAdress(string adress)
        {
            Adress = adress;
        }

        void AddPhoto(string photoRef)
        {
            // TO DO
        }

        /*------------------------------------------------------------------------------------------*/
    }
}
