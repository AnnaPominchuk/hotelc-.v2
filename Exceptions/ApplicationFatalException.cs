﻿using System;

namespace Hotel.Exceptions
{
    public class ApplicationFatalException : Exception
    {
        public ApplicationFatalException(string message)
            : base(message)
        { }
    }
}