﻿using System;

namespace Hotel.Exceptions
{
    public class DuplicatePhoneNumberException : DomainLogicException
    {
        public DuplicatePhoneNumberException(Type t, string phone)
            : base(string.Format("Duplicate phone number of {0} is \"{1}\"", t.Name, phone))
        { }
    }
}