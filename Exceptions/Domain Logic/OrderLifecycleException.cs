﻿using System;

namespace Hotel.Exceptions
{
    public class OrderLifecycleException : DomainLogicException
    {
        private OrderLifecycleException(string message)
            : base(message)
        { }

        public static OrderLifecycleException MakeExecutingWhenNotBooked(Guid orderId)
        {
            return new OrderLifecycleException(
                string.Format(
                    "Order #{0} may be executing in Booked state only",
                    orderId
                )
            );
        }

        public static OrderLifecycleException MakeCanceledWhenInExecuting(Guid orderId)
        {
            return new OrderLifecycleException(
                string.Format(
                    "Order #{0} may be canceled in Booked state only",
                    orderId
                )
            );
        }
    }
}