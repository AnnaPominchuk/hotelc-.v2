﻿using System;

namespace Hotel.Exceptions
{
    public class GuestProcessingLifecycleException : DomainLogicException
    {
        private GuestProcessingLifecycleException(string message)
            : base(message)
        { }

        public static GuestProcessingLifecycleException AddTaskWhenNotExecuting(Guid orderId)
        {
            return new GuestProcessingLifecycleException(
                string.Format(
                    "Task may be added to order #{0} in Executing state only",
                    orderId
                )
            );
        }
    }
}