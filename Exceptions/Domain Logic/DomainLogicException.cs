﻿using System;

namespace Hotel.Exceptions
{
    public class DomainLogicException : Exception
    {
        public DomainLogicException(string message)
            : base(message)
        { }
    }
}