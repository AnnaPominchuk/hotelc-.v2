﻿using System;

namespace Hotel.Exceptions
{
    public class DuplicateLoginException : DomainLogicException
    {
        public DuplicateLoginException(Type t, string login)
            : base(string.Format("Duplicate login of {0} is \"{1}\"", t.Name, login))
        { }
    }
}