﻿using System;

namespace Hotel.Exceptions
{
    public class OrderDateException : DomainLogicException
    {
        public OrderDateException( DateTime start, DateTime end)
            : base(string.Format("{0} - {1} is invalid period", start.ToString(), end.ToString()))
        { }
    }
}
