﻿using System;

namespace Hotel.Exceptions
{
    public class TaskLifecycleException : DomainLogicException
    {
        private TaskLifecycleException(string message)
            : base(message)
        { }

        public static TaskLifecycleException MakeProcessingWhenNotUnassigned(Guid taskId)
        {
            return new TaskLifecycleException(
                string.Format(
                    "Task #{0} may be processing in Unassigned state only",
                    taskId
                )
            );
        }

        public static TaskLifecycleException MakeClosedWhenNotProcessing(Guid taskId)
        {
            return new TaskLifecycleException(
                string.Format(
                    "Task #{0} may be closed in Processing state only",
                    taskId
                )
            );
        }
    }
}