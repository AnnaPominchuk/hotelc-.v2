﻿using Hotel.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.TestApp
{
    class HotelModel
    {
        public List<Account> Accounts { get; private set; }

        public List<Model.Service> Service { get; private set; }

        public List<RoomCategory> Rooms { get; private set; }

        public List<Order> Orders { get; private set; }

        public HotelModel()
        {
            this.Accounts = new List<Account>();
            this.Rooms = new List<RoomCategory>();
            this.Orders = new List<Order>();
            this.Service = new List<Model.Service>();
        }

    }
}
