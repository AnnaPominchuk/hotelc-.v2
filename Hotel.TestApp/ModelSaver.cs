﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Repository.EntityFramework;

using System.Collections.Generic;

namespace Hotel.TestApp
{
    class ModelSaver
    {
        public ModelSaver(HotelDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public void Save(HotelModel model)
        {
            SaveCollection(RepositoryFactory.MakeRoomCategoryRepository(dbContext), model.Rooms);
          //  SaveCollection(RepositoryFactory.MakeOrderRepository(dbContext), model.Orders);
            SaveCollection(RepositoryFactory.MakeAccountRepository(dbContext), model.Accounts);
            SaveCollection(RepositoryFactory.MakeServiceRepository(dbContext), model.Service);
        }


        private void SaveCollection<TRepository, TEntity>(TRepository repository, ICollection<TEntity> collection)
            where TRepository : IRepository<TEntity>
            where TEntity : Utils.Entity
        {
            foreach (TEntity obj in collection)
                repository.Add(obj);

            repository.Commit();
        }

        private HotelDbContext dbContext;
    }
}
