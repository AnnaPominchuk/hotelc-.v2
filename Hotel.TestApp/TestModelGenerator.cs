﻿using Hotel.Model;
using Hotel.Service;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;

namespace Hotel.TestApp
{
    class TestModelGenerator
    {
        public TestModelGenerator(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        /*  public static HotelModel GenerateTestData()
          {
              HotelModel model = new HotelModel();

              GenerateAccounts(model);

              return model;
          }*/

        public void GenerateTestData()
        {
            GenerateAccounts();
            GenerateRoomCategory();
            GenerateRoom();
            GenerateOrder();
        }

        private void GenerateAccounts()
        {
            var acService = unityContainer.Resolve<IAccountService>();
           
            customer1 = acService.RegisterClient("julia", "Julia", "pass1", "0993541769");
            customer2 = acService.RegisterClient("anna",  "Anna", "pass2", "0953475745");
            customer3 = acService.RegisterClient("karina",  "Karina", "pass3" , "0951111111");
            customer4 = acService.RegisterClient("kristina",  "Kristina", "pass4", "0953333333");

            staff1 = acService.RegisterStaff("marina",  "Marina", "pass5", "0954444444");
            staff2 = acService.RegisterStaff("ivan", "Ivan", "pass6", "0955555555");

            administrator = acService.RegisterAdmin("Jack", "Jack", "pass7", "0956666666");

        }

        private void GenerateRoomCategory()
        {
            var service = unityContainer.Resolve<IRoomCategoryService>();

            lux = service.Create(4, 2, 2, 2, "lux");
            semilux = service.Create(3, 1, 0, 2, "semilux");
            economy = service.Create(2, 1, 0, 1, "economy");
        }

        private void GenerateRoom()
        {
            var rService = unityContainer.Resolve<IRoomService>();
            var rcService = unityContainer.Resolve<IRoomCategoryService>();

            room1 = rService.Create(20, "", 20);
            room2 = rService.Create(30, "", 25);
            room3 = rService.Create(18, "", 15);
            room4 = rService.Create(18, "", 15);
            room5 = rService.Create(15, "", 10);
            room6 = rService.Create(15, "", 10);

            rcService.addRoom(lux, room1);
            rcService.addRoom(lux, room2);
            rcService.addRoom(lux, room3);
            rcService.addRoom(lux, room4);
            rcService.addRoom(semilux, room5);
            rcService.addRoom(economy, room6);
        }

        private void GenerateOrder()
        {
            var oService = unityContainer.Resolve<IOrderService>();

            /*order1 = oService.Create( new DateTime(2017, 5, 22), new DateTime(2017, 5, 29), room1);
            order2 = oService.Create(new DateTime(2017, 5, 22), new DateTime(2017, 5, 29), room2);
            order3 = oService.Create(new DateTime(2017, 5, 22), new DateTime(2017, 5, 29), room3);
            order4 = oService.Create(new DateTime(2017, 5, 22), new DateTime(2017, 5, 29), room4);

            acService.connectCustomerOrder(customer1, order1);
            acService.connectCustomerOrder(customer2, order2);
            acService.connectCustomerOrder(customer3, order3);
            acService.connectCustomerOrder(customer4, order4);*/
        }

        Guid customer1, customer2, customer3, customer4;
        Guid staff1, staff2;
        Guid administrator;
        Guid lux, semilux, economy;
        Guid room1, room2, room3, room4, room5, room6;
        Guid order1, order2, order3, order4;

        //private ServiceProvider serviceProvider;
        private IUnityContainer unityContainer;
    }
}
