﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hotel.Model;
using Hotel.Repository;
using Hotel.Repository.EntityFramework;

using System.Collections.Generic;
using System.Data.Entity;

namespace Hotel.TestApp
{
    class ModelRestorer
    {
        public ModelRestorer(HotelDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public HotelModel Restore()
        {
            HotelModel m = new HotelModel();

            RestoreCollection(RepositoryFactory.MakeRoomCategoryRepository(dbContext), m.Rooms);
          //  RestoreCollection(RepositoryFactory.MakeOrderRepository(dbContext), m.Orders);
            RestoreCollection(RepositoryFactory.MakeAccountRepository(dbContext), m.Accounts);
            RestoreCollection(RepositoryFactory.MakeServiceRepository(dbContext), m.Service);

            return m;
        }

        private HotelDbContext dbContext;

        private void RestoreCollection<TEntity>(IRepository<TEntity> repository, ICollection<TEntity> target)
            where TEntity : Utils.Entity
        {
            if ( repository.Count() > 0 )
            foreach (TEntity obj in repository.LoadAll())
                target.Add(obj);
        }
    }
}