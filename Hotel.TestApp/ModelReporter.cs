﻿using System;
using System.Collections.Generic;
using System.IO;
using Hotel.Service;
using Microsoft.Practices.Unity;

namespace Hotel.TestApp
{
    class ModelReporter
    {
        public ModelReporter(IUnityContainer unityContainer, TextWriter output)
        {
            this.unityContainer = unityContainer;
            this.output = output;
        }

        public void GenerateReport()
        {
            ReportCollection("Rooms", unityContainer.Resolve<IRoomService>());
            ReportCollection("Orders", unityContainer.Resolve<IOrderService>());
            ReportCollection("Accounts", unityContainer.Resolve<IAccountService>());
        }

        private void ReportCollection<TDto>(string title, IDomainEntityService<TDto> service)
            where TDto : Dto.DomainEntityDto<TDto>
        {
            output.WriteLine("==== {0} ==== ", title);
            output.WriteLine();

            foreach (var entityId in service.ViewAll())
            {
                output.Write(service.View(entityId));

                output.WriteLine();
                output.WriteLine();
            }

            output.WriteLine();
        }

        private IUnityContainer unityContainer;
        private TextWriter output;
    }
}
