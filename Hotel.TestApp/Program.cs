﻿using Hotel.Dependencies;
using Hotel.Repository.EntityFramework;

using Microsoft.Practices.Unity;

using System;

namespace Hotel.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var dbContext = new HotelDbContext())
                using (var unityContainer = new UnityContainer())
                {
                    dbContext.Database.Initialize(true);

                    ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

                    TestModelGenerator generator = new TestModelGenerator(unityContainer);
                    generator.GenerateTestData();
                }

                using (var dbContext = new HotelDbContext())
                using (var unityContainer = new UnityContainer())
                {
                    ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

                    ModelReporter reportGenerator = new ModelReporter(unityContainer, Console.Out);
                    reportGenerator.GenerateReport();
                }
            }
            catch (Exception e)
            {
                while (e != null)
                {
                    Console.WriteLine("{0}: {1}", e.GetType().FullName, e.Message);
                    //Console.WriteLine( e.StackTrace );

                    e = e.InnerException;
                }
            }
        }
    }
}