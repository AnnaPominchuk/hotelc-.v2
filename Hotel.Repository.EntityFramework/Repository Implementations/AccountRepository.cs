﻿using System;
using System.Data.Entity;
using System.Linq;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    public class AccountRepository : BasicRepository<Account>, IAccountRepository
    {
        public AccountRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.Accounts)
        {
        }

        public Account FindByLogin(string login)
        {
            return GetDBSet().Where(a => a.Login == login).SingleOrDefault();
        }

        public Account FindByPhoneNumber(string phone)
        {
            return GetDBSet().Where(a => a.TelephonNumber == phone).SingleOrDefault();
        }
    }
}