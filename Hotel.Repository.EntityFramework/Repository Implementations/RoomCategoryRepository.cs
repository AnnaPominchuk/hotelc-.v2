﻿using System;
using Hotel.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Repository.EntityFramework
{
    public class RoomCategoryRepository : BasicRepository<RoomCategory>, IRoomCategoryRepository
    {
        public RoomCategoryRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.RoomCategories)
        {
        }
    }
}