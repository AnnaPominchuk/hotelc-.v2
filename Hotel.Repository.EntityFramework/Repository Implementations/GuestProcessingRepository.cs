﻿using System;
using Hotel.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Repository.EntityFramework
{
    public class GuestProcessingRepository : BasicRepository<GuestProcessing>, IGuestProcessingRepository
    {
        public GuestProcessingRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.GuestsProcessing)
        {
        }
    }
}