﻿using System;
using Hotel.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Repository.EntityFramework
{
    public class RoomRepository : BasicRepository<Room>, IRoomRepository
    {
        public RoomRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.Rooms)
        {
        }
    }
}