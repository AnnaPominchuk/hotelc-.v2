﻿using System;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class TaskRepository : BasicRepository<Task>, ITaskRepository
    {
        public TaskRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.Tasks)
        {
        }
    }
}
