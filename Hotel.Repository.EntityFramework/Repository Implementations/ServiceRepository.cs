﻿using System;
using System.Data.Entity;
using System.Linq;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class ServiceRepository : BasicRepository<Service>, IServiceRepository
    {
        public ServiceRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.Service)
        {
        }

        public Service FindByCategory(string service)
        {
            Model.Service.ServiceCategory s = Model.Service.toEnum(service);
            return GetDBSet().Where(a => a.Category == s).SingleOrDefault();
        }
    }
}