﻿using System;
using Hotel.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Repository.EntityFramework
{
    public class PaymentRepository : BasicRepository<Hotel.Model.Payment>, IPaymentRepository
    {
        public PaymentRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.Payments)
        {
        }
    }
}
