﻿using System;
using Hotel.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Repository.EntityFramework
{
    public class OrderRepository : BasicRepository<Hotel.Model.Order>, IOrderRepository
    {
        public OrderRepository(HotelDbContext dbContext)
            : base(dbContext, dbContext.Orders)
        {
        }
    }
}
