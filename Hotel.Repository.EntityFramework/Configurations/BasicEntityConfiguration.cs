﻿using System.Data.Entity.ModelConfiguration;

namespace Hotel.Repository.EntityFramework
{
    abstract class BasicEntityConfiguration<T> : EntityTypeConfiguration<T>
        where T : Hotel.Utils.Entity
    {
        protected BasicEntityConfiguration()
        {
            HasKey(e => e.DatabaseId);
            Property(e => e.Id).IsRequired();
        }
    }
}
