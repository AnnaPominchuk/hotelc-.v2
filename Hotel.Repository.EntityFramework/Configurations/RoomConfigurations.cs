﻿using System.Data.Entity.ModelConfiguration;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class RoomConfigurations : BasicEntityConfiguration<Room>
    {
        public RoomConfigurations()
        {
            Property(a => a.Price).IsRequired();
            Property(a => a.Status).IsRequired();
            Property(a => a.Square).IsRequired();
            Property(a => a.ImageUrl).IsRequired();
        }
    }
}
