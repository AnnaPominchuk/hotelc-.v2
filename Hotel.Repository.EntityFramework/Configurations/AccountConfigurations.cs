﻿using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{ 
    class AccountConfigurations : BasicEntityConfiguration<Account>
    {
        public AccountConfigurations()
        {
            Property(a => a.Name).IsRequired();
            Property(a => a.Login).IsRequired();
            Property(a => a.Password).IsRequired();
        }
    }
}
