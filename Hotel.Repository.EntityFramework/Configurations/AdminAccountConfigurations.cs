﻿using System;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class AdminAccountConfigurations : BasicEntityConfiguration<AdminAccount>
    {
        public AdminAccountConfigurations()
        {
            Property(a => a.Name).IsRequired();
            Property(a => a.Login).IsRequired();
            Property(a => a.Password).IsRequired();
        }
    }
}