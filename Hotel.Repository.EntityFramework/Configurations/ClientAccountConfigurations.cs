﻿using System;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class ClientAccountConfigurations : BasicEntityConfiguration<ClientAccount>
    {
        public ClientAccountConfigurations()
        {
            Property(a => a.Name).IsRequired();
            Property(a => a.Login).IsRequired();
            Property(a => a.Password).IsRequired();

            HasMany(a => a._orders).WithOptional();
        }
    }
}
