﻿using System.Data.Entity.ModelConfiguration;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class GuestProcessingConfigurations : BasicEntityConfiguration<Hotel.Model.GuestProcessing>
    {
        public GuestProcessingConfigurations()
        {
            HasOptional<Order>(a => a.CurrentOrder);
            HasMany<Service>(a => a.Services).WithOptional();
        }
    }
}