﻿using System.Data.Entity.ModelConfiguration;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class TaskConfigurations : BasicEntityConfiguration<Task>
    {
        public TaskConfigurations()
        {
            HasOptional<GuestProcessing>(a => a.processing);
            HasOptional<Service>(a => a.service);
            Property(a => a.status);
        }
    }
}
