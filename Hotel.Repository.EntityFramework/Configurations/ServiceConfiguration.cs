﻿using System;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class ServiceConfiguration : BasicEntityConfiguration<Service>
    {
        public ServiceConfiguration()
        {
            Property(a => a.Price).IsRequired();
            Property(a => a.Category).IsRequired();
        }
    }
}
