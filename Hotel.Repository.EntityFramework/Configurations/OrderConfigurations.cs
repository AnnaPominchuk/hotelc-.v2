﻿using System.Data.Entity.ModelConfiguration;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class OrderConfigurations : BasicEntityConfiguration<Hotel.Model.Order>
    {
        public OrderConfigurations()
        {
            HasRequired<Room>(a => a.room);
            Property(a => a.Status).IsRequired();
            Property(a => a.startTime).IsRequired();
            Property(a => a.endTime).IsRequired();
           // HasOptional<Payment>(a => a.payment);
        }
    }
}