﻿using System;
using Hotel.Model;

namespace Hotel.Repository.EntityFramework
{
    class StaffAccountConfigurations : BasicEntityConfiguration<StaffAccount>
    {
        public StaffAccountConfigurations()
        {
            Property(a => a.Name).IsRequired();
            Property(a => a.Login).IsRequired();
            Property(a => a.Password).IsRequired();

            HasMany(a => a.Tasks).WithOptional();
        }
    }
}
