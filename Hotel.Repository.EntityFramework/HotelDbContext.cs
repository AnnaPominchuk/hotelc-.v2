﻿using Hotel.Model;

using System.Data.Entity;

namespace Hotel.Repository.EntityFramework
{
    public class HotelDbContext : DbContext
    {
        static HotelDbContext()
        {
            Database.SetInitializer(
                new DropCreateDatabaseAlways<HotelDbContext>()
            );
        }

        public HotelDbContext()
        {
            Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));
        }

        public DbSet<Hotel.Model.Order> Orders { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Room> Rooms { get; set; }

        public DbSet<Service> Service { get; set; }

        public DbSet<RoomCategory> RoomCategories { get; set; }

        public DbSet<Payment> Payments { get; set; }

        public DbSet<Task> Tasks { get; set; }

        public DbSet<GuestProcessing> GuestsProcessing { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new RoomConfigurations());
            modelBuilder.Configurations.Add(new OrderConfigurations());
            modelBuilder.Configurations.Add(new StaffAccountConfigurations());
            modelBuilder.Configurations.Add(new ClientAccountConfigurations());
            modelBuilder.Configurations.Add(new AdminAccountConfigurations());
            modelBuilder.Configurations.Add(new ServiceConfiguration());
            modelBuilder.Configurations.Add(new GuestProcessingConfigurations());
            modelBuilder.Configurations.Add(new TaskConfigurations());
        }
    }
}

