﻿using System;
using System.Data.Entity;
using System.Linq;

namespace Hotel.Repository.EntityFramework
{
    public abstract class BasicRepository<T> where T : Hotel.Utils.Entity
    {
        protected BasicRepository(HotelDbContext dbContext, DbSet<T> dbSet)
        {
            this.dbContext = dbContext;
            this.dbSet = dbSet;
        }

        protected HotelDbContext GetDBContext()
        {
            return this.dbContext;
        }

        protected DbSet<T> GetDBSet()
        {
            return this.dbSet;
        }

        public void Add(T obj)
        {
            dbSet.Add(obj);
        }

        public void Delete(T obj)
        {
            dbSet.Remove(obj);
        }

        public T FindByDomainId(Guid domainId)
        {
            return dbSet.Where(e => e.Id == domainId).SingleOrDefault();
        }

        public IQueryable<Guid> SelectAllDomainIds()
        {
            return dbSet.Select(e => e.Id);
        }

        public void StartTransaction()
        {
            this.dbContext.Database.BeginTransaction();
        }

        public void Commit()
        {
            this.dbContext.ChangeTracker.DetectChanges();
            this.dbContext.SaveChanges();
            this.dbContext.Database.CurrentTransaction.Commit();
        }

        public IQueryable<T> LoadAll()
        {
            return dbSet;
        }

        public T Load(int id)
        {
            return dbSet.Find(id);
        }

        public int Count()
        {
            return dbSet.Count();
        }

        private HotelDbContext dbContext;
        private DbSet<T> dbSet;
    }
}

