﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Repository.EntityFramework
{
    public static class RepositoryFactory
    {
        public static IRoomRepository MakeRoomRepository(HotelDbContext dbContext)
        {
            return new RoomRepository(dbContext);
        }

        public static IOrderRepository MakeOrderRepository(HotelDbContext dbContext)
        {
            return new OrderRepository(dbContext);
        }

        public static IAccountRepository MakeAccountRepository(HotelDbContext dbContext)
        {
            return new AccountRepository(dbContext);
        }

        public static IServiceRepository MakeServiceRepository(HotelDbContext dbContext)
        {
            return new ServiceRepository(dbContext);
        }

        public static IGuestProcessingRepository MakeGuestProcessingRepository(HotelDbContext dbContext)
        {
            return new GuestProcessingRepository(dbContext);
        }

        public static ITaskRepository MakeTaskRepository(HotelDbContext dbContext)
        {
            return new TaskRepository(dbContext);
        }

        public static IPaymentRepository MakePaymentRepository(HotelDbContext dbContext)
        {
            return new PaymentRepository(dbContext);
        }

        public static IRoomCategoryRepository MakeRoomCategoryRepository(HotelDbContext dbContext)
        {
            return new RoomCategoryRepository(dbContext);
        }
    }
}
