﻿using System;

using Microsoft.AspNetCore.Http;


namespace Hotel.Web
{
    public class SessionHelper
    {
        public SessionHelper(ISession session)
        {
            this.session = session;
        }

        public Guid? GetOrderId()
        {
            string result = session.GetString(OrderId);
            if (result == null || result.Length == 0)
                return null;

            return Guid.Parse(result);
        }

        public void SetOrderId(Guid orderId)
        {
            session.SetString(OrderId, orderId.ToString());
        }


        private static readonly string OrderId = "ORDER_ID";

        private ISession session;
    }
}