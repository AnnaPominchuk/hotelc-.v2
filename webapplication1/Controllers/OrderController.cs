﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Hotel.Dto;
using Hotel.Service;
using Hotel.Web.Models;


namespace Hotel.Web.Controllers
{
    public class OrderController : Controller
    {

        public IActionResult Index()
        {
           // OrderDto orderDto = CurrentOrder();

            var viewModel = new OrderViewModel();
       //     viewModel.Order = orderDto;

            return View(viewModel);
        }


        [HttpPost]
        public IActionResult Index(
            DateTime starttime, 
            DateTime endtime, 
            Guid _room
        )
        {
            //OrderDto orderDto = CurrentOrder();

            Guid orderId = orderService.Create(
                                starttime,
                                endtime,
                                _room
                           );

            var sessionHelper = new SessionHelper(HttpContext.Session);
            sessionHelper.SetOrderId(orderId);

            return new RedirectToActionResult("Index", "Index", null);
        }


        //private OrderDto CurrentOrder()
        //{
        //  //  SessionHelper helper = new SessionHelper(HttpContext.Session);
        //   // Guid? cartId = helper.GetCartId();
        //    return (cartId.HasValue) ? cartService.View(cartId.Value) : null;
        //}

        [Dependency]
        protected IOrderService orderService { get; set; }
    }
}