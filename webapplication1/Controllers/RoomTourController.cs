﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Hotel.Dto;
using Hotel.Service;
using Hotel.Web.Models;


namespace Hotel.Web.Controllers
{
    public class RoomTourController : Controller
    {
        public IActionResult Index()
        {
            var viewModel = new RoomTourViewModel();
            viewModel.Products = GetRoomCategorylist();

            return View(viewModel);
        }

        private IList<RoomCategoryDto> GetRoomCategorylist()
        {
            IList<Guid> productIds = roomCategoryService.ViewAll();

            IList<RoomCategoryDto> result = new List<RoomCategoryDto>();
            foreach (var productId in productIds)
                result.Add(roomCategoryService.View(productId));

            return result.OrderBy(p => p.Name).ToList();
        }


        [Dependency]
        protected IRoomCategoryService roomCategoryService { get; set; }
    }
}