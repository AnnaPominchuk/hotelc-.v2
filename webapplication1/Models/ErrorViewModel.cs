﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Web.Models
{
    public class ErrorViewModel
    {
           public string ErrorTitle { get; set; }
    
           public string ErrorMessage { get; set; }
    
    }

}
