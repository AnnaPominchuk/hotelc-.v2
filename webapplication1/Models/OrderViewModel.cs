﻿using Hotel.Dto;

namespace Hotel.Web.Models
{
    public class OrderViewModel
    {
        public OrderDto Order { get; set; }
    }
}