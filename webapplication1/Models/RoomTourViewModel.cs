﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dto;

namespace Hotel.Web.Models
{
    public class RoomTourViewModel
    {
        public IList<RoomCategoryDto> Products { get; set; }
    }
}
