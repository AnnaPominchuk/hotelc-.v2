﻿using System;
using Hotel.Model;

/********************************************************************************************/

namespace Hotel.Repository
{
    public interface IRoomCategoryRepository : IRepository<RoomCategory>
    {
    }
}

/********************************************************************************************/
