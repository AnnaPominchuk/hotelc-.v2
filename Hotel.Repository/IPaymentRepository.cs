﻿using Hotel.Model;

namespace Hotel.Repository
{
    public interface IPaymentRepository : IRepository<Payment>
    {
    }
}
