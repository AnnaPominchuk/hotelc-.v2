﻿using Hotel.Model;

namespace Hotel.Repository
{
    public interface ITaskRepository : IRepository<Task>
    {
    }
}
