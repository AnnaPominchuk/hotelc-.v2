﻿using System;
using Hotel.Model;

namespace Hotel.Repository
{
    public interface IAccountRepository : IRepository<Account>
    {
        Account FindByLogin(string login);

        Account FindByPhoneNumber(string login);
    }
}
