﻿using System;
using Hotel.Model;

/********************************************************************************************/

namespace Hotel.Repository
{
    public interface IRoomRepository : IRepository<Room>
    {
    }
}

/********************************************************************************************/
