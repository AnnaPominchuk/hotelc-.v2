﻿using Hotel.Model;

namespace Hotel.Repository
{
    public interface IServiceRepository : IRepository<Service>
    {
        Service FindByCategory(string service);
    }
}
