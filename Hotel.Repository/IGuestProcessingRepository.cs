﻿using Hotel.Model;

namespace Hotel.Repository
{
    public interface IGuestProcessingRepository : IRepository<GuestProcessing>
    {
    }
}
