﻿using System;
using Hotel.Model;

/********************************************************************************************/

namespace Hotel.Repository
{
    public interface IOrderRepository : IRepository<Hotel.Model.Order>
    {
    }
}

/********************************************************************************************/
