﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Hotel.Service.Impl
{
    public class RoomService : IRoomService
    {
        public RoomService(
            IRoomRepository roomRepository
        )
        {
            this.roomRepository = roomRepository;
        }

        public IList<Guid> ViewAll()
        {
            return roomRepository.SelectAllDomainIds().ToList();
        }

        public RoomDto View(Guid Id)
        {
            Room a = ResolveRoom(Id);
            return a.ToDto();
        }

        public void AddPhoto(Guid id, string Url)
        {
            Room room = ResolveRoom(id);

            room.ImageUrl = Url;
        }

        public void ChangePrice(Guid id, int price)
        {
            Room room = ResolveRoom(id);

            room.Price = price;
        }

        public RoomDto ShowRoomInfo(Guid id)
        {
            Room room = ResolveRoom(id);

            return room.ToDto();
        }

        public Guid Create(int square, string imageUrl, int price)
        {
            Room r = new Room(Guid.NewGuid(), square, imageUrl, price);
            roomRepository.Add(r);

            return r.Id;
        }

        private Room ResolveRoom(Guid accountId)
        {
            return ServiceUtils.ResolveEntity(roomRepository, accountId);
        }

        private IRoomRepository roomRepository;
    }
}