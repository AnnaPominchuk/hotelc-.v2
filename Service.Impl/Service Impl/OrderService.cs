﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Dto;
using Hotel.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;

namespace Hotel.Service.Impl
{
    public class OrderService : IOrderService
    {
        public OrderService(
            IOrderRepository orderRepository,
            IRoomRepository roomRepository
        )
        {
            this.orderRepository = orderRepository;
            this.roomRepository = roomRepository;
        }

        public IList<Guid> ViewAll()
        {
            return orderRepository.SelectAllDomainIds().ToList();
        }

        public OrderDto View(Guid orderId)
        {
            Order a = ResolveOrder(orderId);
            return a.ToDto();
        }

        public void ConfirmOrderPlacement(Guid id)
        {
            orderRepository.StartTransaction();

            Order order = ResolveOrder(id);

            if ( order.Status.ToString() != "executing")
                throw OrderLifecycleException.MakeExecutingWhenNotBooked(id);

            order.Confirm();

            orderRepository.Commit();
        }

        //  void SpecifyCardInfo(Guid id); ???

        public void CancelReservation(Guid id)
        {
           
            Order order = ResolveOrder(id);

            if (order.Status.ToString() == "executing")
                throw OrderLifecycleException.MakeCanceledWhenInExecuting(id);

            order.Cancel();

        }

        public Guid Create(DateTime starttime, DateTime endtime, Guid _room)
        {
            Room room = ResolveRoom(_room);

            if (starttime > endtime)
                throw new OrderDateException(starttime, endtime);

            Order r = new Order(Guid.NewGuid(), starttime, endtime, room);
            orderRepository.Add(r);

            return r.Id;
        }

        private Order ResolveOrder(Guid accountId)
        {
            return ServiceUtils.ResolveEntity(orderRepository, accountId);
        }

        private Room ResolveRoom(Guid accountId)
        {
            return ServiceUtils.ResolveEntity(roomRepository, accountId);
        }

        private IOrderRepository orderRepository;
        private IRoomRepository roomRepository;
    }
}