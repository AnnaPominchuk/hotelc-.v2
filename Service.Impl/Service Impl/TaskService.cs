﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Dto;
using Hotel.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Hotel.Service.Impl
{
    public class TaskService : ITaskService
    {
        public TaskService(
            ITaskRepository taskRepository,
            IGuestProcessingRepository guestProcessingRepository,
            IServiceRepository serviceRepository
        )
        {
            this.taskRepository = taskRepository;
            this.guestProcessingRepository = guestProcessingRepository;
            this.serviceRepository = serviceRepository;
        }

    public IList<Guid> ViewAll()
        {
            return taskRepository.SelectAllDomainIds().ToList();
        }

        public TaskDto View(Guid Id)
        {
            Task task = ResolveTask(Id);
            return task.ToDto();
        }

        public string ViewStatus(Guid id)
        {
            Task task = ResolveTask(id);

            return task.status.ToString();
        }

        public Guid AddNewTask(GuestProcessingDto _processing, ServiceDto _service)
        {
            GuestProcessing g = ResolveGuestProcessing(_processing.DomainId);
            Model.Service s = ResolveService(_service.DomainId);

            Task task = new Task(Guid.NewGuid(), g, s);
            taskRepository.Add(task);

            return task.Id;
        }

        private Task ResolveTask(Guid Id)
        {
            return ServiceUtils.ResolveEntity(taskRepository, Id);
        }

        private GuestProcessing ResolveGuestProcessing(Guid orderId)
        {
            return ServiceUtils.ResolveEntity(guestProcessingRepository, orderId);
        }

        private Model.Service ResolveService(Guid orderId)
        {
            return ServiceUtils.ResolveEntity(serviceRepository, orderId);
        }

        public void CloseTask(Guid id)
        {
            Task task = ResolveTask(id);

            if ( task.status.ToString() != "processing")
                throw TaskLifecycleException.MakeClosedWhenNotProcessing(id);

            task.close();
        }

        public void ProgressTask(Guid id)
        {
            Task task = ResolveTask(id);

            if (task.status.ToString() != "unassigned")
                throw TaskLifecycleException.MakeProcessingWhenNotUnassigned(id);

            task.progress();
        }

        private ITaskRepository taskRepository;
        private IGuestProcessingRepository guestProcessingRepository;
        private IServiceRepository serviceRepository;
    }
}
