﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Hotel.Service.Impl
{
    public class RoomCategoryService : IRoomCategoryService
    {
        public RoomCategoryService(
            IRoomCategoryRepository roomCategoryRepository,
            IRoomRepository roomRepository
        )
        {
            this.roomCategoryRepository = roomCategoryRepository;
            this.roomRepository = roomRepository;
        }

        public IList<Guid> ViewAll()
        {
            return roomCategoryRepository.SelectAllDomainIds().ToList();
        }

        public RoomCategoryDto View(Guid Id)
        {
            RoomCategory rc = ResolveRoomCategory(Id);
            return rc.ToDto();
        }

        public void ChangeBedsNumber(Guid roomCategory, int newBedsNumber)
        {
            RoomCategory rc = ResolveRoomCategory(roomCategory);
            rc.changeBedsNumber(newBedsNumber);
        }

        public void ChangeSingleBedsNumber(Guid roomCategory, int newSingleBedsNumber)
        {
            RoomCategory rc = ResolveRoomCategory(roomCategory);
            rc.changeSingleBedsNumber(newSingleBedsNumber);
        }

        //public Guid AddInfoCard(int RoomsNumber, int BathroomsNumber, int SingleBedsNumber, int BedsNumber)
        //{
        //    RoomCategory rc = new RoomCategory(Guid.NewGuid(), RoomsNumber, BathroomsNumber, SingleBedsNumber, BedsNumber, name);
        //    roomCategoryRepository.Add(rc);

        //    return rc.Id;
        //}

        public Guid Create(int roomsNumber, int bathroomsNumber, int singleBedsNumber, int bedsNumber, string name)
        {
            RoomCategory r = new RoomCategory(Guid.NewGuid(), roomsNumber, bathroomsNumber, singleBedsNumber, bedsNumber, name);
            roomCategoryRepository.Add(r);

            return r.Id;
        }

        public void addRoom(Guid roomCategory, Guid roomId)
        {
            RoomCategory rc = ResolveRoomCategory(roomCategory);
            Room room = ResolveRoom(roomId);
            rc.Rooms.Add(room);
        }

        private RoomCategory ResolveRoomCategory(Guid Id)
        {
            return ServiceUtils.ResolveEntity(roomCategoryRepository, Id);
        }

        private Room ResolveRoom(Guid accountId)
        {
            return ServiceUtils.ResolveEntity(roomRepository, accountId);
        }

        private IRoomCategoryRepository roomCategoryRepository;
        private IRoomRepository roomRepository;
    }
}
