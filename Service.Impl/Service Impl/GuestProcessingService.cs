﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Dto;
using Hotel.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Hotel.Service.Impl
{
    public class GuestProcessingService : IGuestProcessingService
    {
        public GuestProcessingService(
            IGuestProcessingRepository _guestProcessingRepository,
            ITaskRepository _taskRepository,
            IServiceRepository _serviceRepository
        )
        {
            this.guestProcessingRepository = _guestProcessingRepository;
            this.taskRepository = _taskRepository;
            this.serviceRepository = _serviceRepository;
        }

        public IList<Guid> ViewAll()
        {
            return guestProcessingRepository.SelectAllDomainIds().ToList();
        }

        public GuestProcessingDto View(Guid accountId)
        {
            GuestProcessing a = ResolveGuestProcessing(accountId);
            return a.ToDto();
        }

        public TaskDto OrderDishes(Guid id)
        {
            GuestProcessing gp = ResolveGuestProcessing(id);
            Model.Service s = serviceRepository.FindByCategory("eating");

            Task task = new Task(Guid.NewGuid(), gp, s);

            if( gp.CurrentOrder.Status.ToString() != "executing")
                throw GuestProcessingLifecycleException.AddTaskWhenNotExecuting(id);

            taskRepository.Add(task);


            return task.ToDto();

        }

        public TaskDto OrderDrinks(Guid id)
        {
            GuestProcessing gp = ResolveGuestProcessing(id);
            Hotel.Model.Service s = serviceRepository.FindByCategory("drinking");

            Task task = new Task(Guid.NewGuid(), gp, s);

            if (gp.CurrentOrder.Status.ToString() != "executing")
                throw GuestProcessingLifecycleException.AddTaskWhenNotExecuting(id);

            taskRepository.Add(task);
            

            return task.ToDto();
        }

        private GuestProcessing ResolveGuestProcessing(Guid orderId)
        {
            return ServiceUtils.ResolveEntity(guestProcessingRepository, orderId);
        }

        private GuestProcessing ResolveOrder(Guid orderId)
        {
            return ServiceUtils.ResolveEntity(guestProcessingRepository, orderId);
        }

        private IGuestProcessingRepository guestProcessingRepository;
        private ITaskRepository taskRepository;
        private IServiceRepository serviceRepository;
    }
}