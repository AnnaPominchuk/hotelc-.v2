﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Hotel.Service.Impl
{
    public class PaymentService : IPaymentService
    {
        public PaymentService(
            IPaymentRepository paymentRepository
        )
        {
            this.paymentRepository = paymentRepository;
        }

        public IList<Guid> ViewAll()
        {
            return paymentRepository.SelectAllDomainIds().ToList();
        }

        public PaymentDto View(Guid Id)
        {
            Payment a = ResolvePayment(Id);
            return a.ToDto();
        }

        public void SelectPaymentMethod( Guid id, string method )
        {
            paymentRepository.StartTransaction();
            Payment Payment = ResolvePayment(id);

            switch ( method )
            {
                case "cash" :
                    Payment.Status = Payment.PaymentMethod.cash;
                    break;
                case "cashless":
                    Payment.Status = Payment.PaymentMethod.cashless;
                    break;
                default:
                    break; // excetion
            }

            paymentRepository.Commit();
        }

        public bool SpecifyCardInfo(string CardNumber, string CVVcode)
        {
            if (CardNumber.Length != 10 && CVVcode.Length != 3)
                return false;

            return true;
        }

        public void ConfirmCardPayment(Guid id)
        {
            Payment Payment = ResolvePayment(id);

            Payment.Confirm();
        }

        private Payment ResolvePayment(Guid accountId)
        {
            return ServiceUtils.ResolveEntity(paymentRepository, accountId);
        }

        private IPaymentRepository paymentRepository;
    }
}