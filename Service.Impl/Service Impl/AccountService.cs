﻿using Hotel.Model;
using Hotel.Repository;
using Hotel.Dto;
using Hotel.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;

namespace Hotel.Service.Impl
{
    public class AccountService : IAccountService
    {
        public AccountService(
            IAccountRepository accountRepository,
            IOrderRepository orderRepository
        )
        {
            this.accountRepository = accountRepository;
            this.orderRepository = orderRepository;
        }

        public IList<Guid> ViewAll()
        {
            return accountRepository.SelectAllDomainIds().ToList();
        }

        public AccountDto View(Guid accountId)
        {
            Account a = ResolveAccount(accountId);
            return a.ToDto();
        }

        public AccountDto Identify(string login, string password)
        {
            Account a = accountRepository.FindByLogin(login);
            if (a == null)
                return null;

            if (!a.CheckPassword(password))
                return null;

            return a.ToDto();
        }

        public Guid RegisterClient(string name, string login, string password, string phone)
        {
            Account a = accountRepository.FindByLogin(login);
            if (a != null)
                throw new DuplicateLoginException(typeof(Account), login);

            Account a2 = accountRepository.FindByPhoneNumber(phone);
            if (a2 != null)
                throw new DuplicatePhoneNumberException(typeof(Account), phone);

            ClientAccount Account = new ClientAccount(Guid.NewGuid(), name, login, password, phone);
            accountRepository.Add(Account);

            return Account.Id;
        }

        public Guid RegisterAdmin(string name, string login, string password, string phone)
        {
            Account a = accountRepository.FindByLogin(login);
            if (a != null)
                throw new DuplicateLoginException(typeof(Account), login);

            Account a2 = accountRepository.FindByPhoneNumber(phone);
            if (a2 != null)
                throw new DuplicatePhoneNumberException(typeof(Account), phone);

            AdminAccount Account = new AdminAccount(Guid.NewGuid(), name, login, password, phone);
            accountRepository.Add(Account);

            return Account.Id;
        }

        public Guid RegisterStaff(string name, string login, string password, string phone)
        {
            Account a = accountRepository.FindByLogin(login);
            if (a != null)
                throw new DuplicateLoginException(typeof(Account), login);

            Account a2 = accountRepository.FindByPhoneNumber(phone);
            if (a2 != null)
                throw new DuplicatePhoneNumberException(typeof(Account), phone);

            StaffAccount Account = new StaffAccount(Guid.NewGuid(), name, login, password, phone);
            accountRepository.Add(Account);


            return Account.Id;
        }

        public void ChangeName(Guid accountId, string newLogin)
        {
            Account a = ResolveAccount(accountId);
            a.Login = newLogin;
        }


        public void DeleteAccount(Guid accountId)
        {
            
        }

        public void connectCustomerOrder(Guid customerId, Guid orderId)
        {

            Order o = ResolveOrder(orderId);

            Account a = ResolveAccount(customerId);
            ClientAccount client = a as ClientAccount;

            client._orders.Add(o);

        }

        private Account ResolveAccount(Guid accountId)
        {
            return ServiceUtils.ResolveEntity(accountRepository, accountId);
        }

        private Order ResolveOrder(Guid accountId)
        {
            return ServiceUtils.ResolveEntity(orderRepository, accountId);
        }

        private IOrderRepository orderRepository;
        private IAccountRepository accountRepository;
    }
}