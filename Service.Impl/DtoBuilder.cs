﻿using Hotel.Model;
using Hotel.Dto;

using System.Collections.Generic;

namespace Hotel.Service.Impl
{
    static class DtoBuilder
    {
        public static AccountDto ToDto(this Account account)
        {
            return new AccountDto(account.Id, account.Name, account.Login, account.TelephonNumber);
        }

        public static GuestProcessingDto ToDto(this GuestProcessing gprocess)
        {
            var itemDtos = new List<ServiceDto>();
            foreach (var item in gprocess.Services)
                itemDtos.Add(item.ToDto());

            return new GuestProcessingDto(
                gprocess.Id,
                gprocess.CurrentOrder.ToDto(),
                itemDtos
            );
        }

        public static OrderDto ToDto(this Order order)
        {
            return new OrderDto(
                order.Id,
                order.startTime,
                order.endTime,
                order.Status.ToString(),
                order.room.ToDto()
            );
        }

        public static PaymentDto ToDto(this Payment payment)
        {
            return new PaymentDto(payment.Id, payment.Price, payment.Exist);
        }

        public static RoomCategoryDto ToDto(this RoomCategory roomCat )
        {
            return new RoomCategoryDto(
                  roomCat.Id
                , roomCat.RoomsNumber
                , roomCat.BathroomsNumber
                , roomCat.SingleBedsNumber
                , roomCat.BedsNumber
                , roomCat.Name
           );
        }

        public static RoomDto ToDto(this Room room)
        {
            return new RoomDto(
                   room.Id,
                   room.ImageUrl,
                   room.Price,
                   room.Square,
                   room.Status.ToString()
            );
        }

        public static ServiceDto ToDto(this Model.Service service )
        {
            return new ServiceDto(
                 service.Id,
                 service.Category.ToString(),
                 service.Price
            );
        }

        public static TaskDto ToDto(this Task task)
        {
            return new TaskDto(
                  task.Id,
                   task.processing.ToDto(),
                   task.service.ToDto(),
                   task.status.ToString()
            );
        } 
    }
}

