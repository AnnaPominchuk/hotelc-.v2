﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hotel.Model;

namespace Hotel.Tests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestClientAccount()
        {
            string name = "George" ;
            string login = "George2000";
            string passwordHash = "spinach";
            string telephonNumber = "101234";
            Account newClient = new ClientAccount(Guid.NewGuid(), name, login, passwordHash, telephonNumber);
            Assert.AreEqual(newClient.Name, name);
            Assert.AreEqual(newClient.Login, login);
            Assert.AreEqual(newClient.Password, passwordHash);
            Assert.AreEqual(newClient.TelephonNumber, telephonNumber);
        }

        [TestMethod]
        public void TestAdminAccount()
        {
            string name = "Leo";
            string login = "Leo";
            string passwordHash = "spinach";
            string telephonNumber = "101234";
            Account newAdmin = new ClientAccount(Guid.NewGuid(), name, login, passwordHash, telephonNumber);
            Assert.AreEqual(newAdmin.Name, name);
            Assert.AreEqual(newAdmin.Login, login);
            Assert.AreEqual(newAdmin.Password, passwordHash);
            Assert.AreEqual(newAdmin.TelephonNumber, telephonNumber);
        }

        [TestMethod]
        public void TestStaffAccount()
        {
            string name = "George";
            string login = "George2000";
            string passwordHash = "spinach";
            string telephonNumber = "101234";
            Account newClient = new StaffAccount(Guid.NewGuid(), name, login, passwordHash, telephonNumber);
            Assert.AreEqual(newClient.Name, name);
            Assert.AreEqual(newClient.Login, login);
            Assert.AreEqual(newClient.Password, passwordHash);
            Assert.AreEqual(newClient.TelephonNumber, telephonNumber);
        }

        [TestMethod]
        public void TestRoom()
        {
            int square = 30;
            string imageUrl = "https://www.pinterest.com/pin/316166836331681911";
            int price = 25;
            Room room = new Room(Guid.NewGuid(), square, imageUrl, price);
            Assert.AreEqual(room.Square, square);
            Assert.AreEqual(room.ImageUrl, imageUrl);
            Assert.AreEqual(room.Price, price);
        }

        [TestMethod]
        public void TestRoomCategory()
        {
            int roomsNumber = 2;
            int bathroomsNumber = 1;
            int singleBedsNumber = 0;
            int bedsNumber = 1;
            string name = "standart";
            RoomCategory newRoomCategory = new RoomCategory(Guid.NewGuid(), roomsNumber, bathroomsNumber, singleBedsNumber, bedsNumber, name);

            Assert.AreEqual(newRoomCategory.RoomsNumber, roomsNumber);
            Assert.AreEqual(newRoomCategory.BathroomsNumber, bathroomsNumber);
            Assert.AreEqual(newRoomCategory.SingleBedsNumber, singleBedsNumber);
            Assert.AreEqual(newRoomCategory.BedsNumber, bedsNumber);
            Assert.AreEqual(newRoomCategory.Name, name);
        }

        [TestMethod]
        public void TestPayment()
        {
            Payment payment = new Payment(Guid.NewGuid());
            payment.makePayment();
            Assert.AreEqual(payment.Exist, true);
            Assert.IsTrue(payment.Exist);
        }

        [TestMethod]
        public void TestCheckPayment()
        {
            Payment payment = new Payment(Guid.NewGuid());
            payment.makePayment();
            Assert.AreEqual(payment.Exist, true);
            Assert.IsTrue(payment.checkPayment());
        }

        [TestMethod]
        public void TestPaymentConfirm()
        {
            Payment payment = new Payment(Guid.NewGuid());
            payment.Confirm();
            Assert.AreEqual(payment.Exist, true);
            Assert.IsTrue(payment.checkPayment());
        }

        [TestMethod]
        public void TestChangePrice()
        {
            int square = 30;
            string imageUrl = "https://www.pinterest.com/pin/316166836331681911";
            int price = 25;
            int new_price = 30;
            Room room = new Room(Guid.NewGuid(), square, imageUrl, price);
            room.ChangePrice(new_price);
            Assert.AreEqual(room.Price, new_price);
        }

        [TestMethod]
        public void TestChangeBedsNumber()
        {
            int roomsNumber = 2;
            int bathroomsNumber = 1;
            int singleBedsNumber = 0;
            int bedsNumber = 1;
            string name = "standart";
            RoomCategory newRoomCategory = new RoomCategory(Guid.NewGuid(), roomsNumber, bathroomsNumber, singleBedsNumber, bedsNumber, name);

            int newbedsNumber = 3;
            newRoomCategory.changeBedsNumber(newbedsNumber);

            Assert.AreEqual(newRoomCategory.BedsNumber, newbedsNumber);
        }

        [TestMethod]
        public void TestChangeSingleBedsNumber()
        {
            int roomsNumber = 2;
            int bathroomsNumber = 1;
            int singleBedsNumber = 0;
            int bedsNumber = 1;
            string name = "standart";
            RoomCategory newRoomCategory = new RoomCategory(Guid.NewGuid(), roomsNumber, bathroomsNumber, singleBedsNumber, bedsNumber, name);

            int newchangeSingleBedsNumber = 3;
            newRoomCategory.changeSingleBedsNumber(newchangeSingleBedsNumber);

            Assert.AreEqual(newRoomCategory.SingleBedsNumber, newchangeSingleBedsNumber);
        }

        [TestMethod]
        public void TestService()
        {
            int price = 10;
            Service Service = new Service(Guid.NewGuid(), Service.ServiceCategory.spa, price);
            
            Assert.AreEqual(Service.Price, price);
            Assert.AreEqual(Service.Category, Service.ServiceCategory.spa);
        }

        [TestMethod]
        public void TestOrder()
        {
            int square = 30;
            string imageUrl = "https://www.pinterest.com/pin/316166836331681911";
            int price = 25;
            Room room = new Room(Guid.NewGuid(), square, imageUrl, price);

            DateTime starttime = new DateTime(2017, 12, 25);
            DateTime endtime = new DateTime(2018, 02, 01);

            Order newOrder = new Order(Guid.NewGuid(), starttime, endtime, room);
            Assert.AreEqual(newOrder.Status, Order.OrderStatus.booked);
        }

        [TestMethod]
        public void TestOrderCancel()
        {
            int square = 30;
            string imageUrl = "https://www.pinterest.com/pin/316166836331681911";
            int price = 25;
            Room room = new Room(Guid.NewGuid(), square, imageUrl, price);

            DateTime starttime = new DateTime(2017, 12, 25);
            DateTime endtime = new DateTime(2018, 02, 01);

            Order newOrder = new Order(Guid.NewGuid(), starttime, endtime, room);
            newOrder.Cancel();
            Assert.AreEqual(newOrder.Status, Order.OrderStatus.canceled);
        }
    }
}
